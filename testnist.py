#!/usr/bin/env python3

import os, sys
import json
import requests
import sqlite3
from mastodon import Mastodon
import settings as s


EXIT_FAILURE = 1

scriptdir = os.path.dirname(os.path.realpath(__file__))
BIBLE_LINE_DB_PATH = os.path.join(scriptdir, "bibledb")
BIBLE_VERSE_DB_PATH = os.path.join(scriptdir, "bible-sqlite.db")


# Find nearest verse number corresponding to line number
def findSearchVerse(search):
  conn = sqlite3.connect(BIBLE_LINE_DB_PATH)
  c = conn.cursor()
  c.execute("select id,book,startline,endline,versepos from Verses where startline >= :start limit 1", {"start": search})
  result = c.fetchone()
  if result:
    #print(result)
    ch, vr = result[4].split(":")
    temp = {"book": result[1], "chapter": ch, "verse": vr}
    #print(temp)
  
  conn.close()
  return temp
  """book = result["book"].ljust(2, "0")
  ch, vr = result["versepos"].split(":")
  chapter = ch.ljust(3, "0")
  verse = vr.ljust(3, "0")
  conn.close()
  return "".join([book, chapter, verse])
  """


# Search full bible SQL file for adjacent verses
def findFinalVerseOutput(search):
  book = str(search["book"]).rjust(2, "0")
  chapter = str(search["chapter"]).rjust(3, "0")
  verse = str(search["verse"]).rjust(3, "0")
  verseid = "".join([book, chapter, verse])
  print(f"Verse id: {verseid}")

  conn = sqlite3.connect(BIBLE_VERSE_DB_PATH)
  c = conn.cursor()
  
  c.execute("select verse.b,verse.c,verse.v,verse.t,book.n,verse.id from t_kjv verse inner join key_english book on verse.b=book.b where verse.b = :book and verse.id >= :start limit 4", {"start": verseid, "book": book})
  result = c.fetchall()
  finalOutput = []
  for row in result:
    finalOutput.append({"book_name": row[4], "book_id": row[0], "chapter": row[1], "verse_num": row[2], "verse_id": row[5], "verse_text": row[3]})

  conn.close()
  return finalOutput


def generateStartText(verses):
  startText = "{}".format(verses[0]["book_name"])
  return startText


def generateFinalOutput(verses):
  finalLines = []
  startText = generateStartText(verses)
  finalLines.append(startText)
  for bibverse in verses:
    current_line = "{}:{} {}".format(bibverse["chapter"], bibverse["verse_num"], bibverse["verse_text"])
    #print(current_line, end=" ")
    finalLines.append(current_line)

  finalOutput = f"Timestamp: {timestamp}\n"
  finalOutput += f"Pulse Index: {pulseIndex}\n"
  finalOutput += "\n"
  finalOutput += " ".join(finalLines)
  return finalOutput


# Grab latest NIST Beacon value
r = requests.get('https://beacon.nist.gov/beacon/2.0/pulse/last')
output = None
if r:
  output = json.loads(r.text)

if not output:
  print("No JSON object was retrieved from NIST", file=sys.stderr)
  sys.exit(EXIT_FAILURE)

#print(output["pulse"]["outputValue"])
#print(output["pulse"])

#nistRandNum = "094F98DFE4D8339AD05E12C64C3A0435D9F8861C590DE082DF058DF2A5AADCEE57892D385EA30B7C9FF04C9C733D3400BBB64F0A496C9AFF690FCF870F33CDAE"
nistRandNum = output["pulse"]["outputValue"]
timestamp = output["pulse"]["timeStamp"]
pulseIndex = output["pulse"]["pulseIndex"]
print(f"Beacon: {nistRandNum}")
print(f"Timestamp: {timestamp}")
print(f"Pulse Index: {pulseIndex}")
searchNum = []
numchars = 0

# Go through string to find first 5 decimal numbers
for i, c in enumerate(nistRandNum):
  #print(c)
  if c.isnumeric():
    searchNum.append(c)
    numchars += 1

  # Check for length of 5
  if numchars >= 5:
    break

candidateLen = len(searchNum)
# Check that a 5 digit number was found
if candidateLen < 5:
  print("5 digit number not found in beacon", file=sys.stderr)
  sys.exit(EXIT_FAILURE)

# Remove padded 0 chars
while searchNum[0] == "0":
  searchNum.pop(0)

# Check new candidate length
candidateLen = len(searchNum)
# Make sure a non-zero number was obtained (not 00000)
if candidateLen == 0:
  print("Number not found in beacon", file=sys.stderr)
  sys.exit(EXIT_FAILURE)

# Check line number for appropriate verse
finalSearch = int("".join(searchNum))
print(f"Line search: {finalSearch}")


# Use verse info to find adjacent verses in a book
verse_info = findSearchVerse(finalSearch)
finalVerseInfo = findFinalVerseOutput(verse_info)


# Generate and trim final output
finalOutput = generateFinalOutput(finalVerseInfo)
## Loop to find number of verses with text length below set char limit
while (len(finalOutput) > s.CHAR_LIMIT and len(finalVerseInfo) > 1):
  finalVerseInfo = finalVerseInfo[:-1]
  finalOutput = generateFinalOutput(finalVerseInfo)

# Check if final output is over char limit. Can be hit with only one verse
if len(finalOutput) > s.CHAR_LIMIT:
  finalOutput = "{}...".format(finalOutput[:s.CHAR_LIMIT-3])

# Print lines
startText = generateStartText(finalVerseInfo)
print(startText, end=" ")

print()
print()
print()

print(finalOutput)

# Set up Mastodon
mastodon = Mastodon(
    access_token = s.ACCESS_TOKEN,
    api_base_url = s.API_BASE_URL,
    #debug_requests = True,
)


mastodon.status_post(finalOutput, visibility="public")


